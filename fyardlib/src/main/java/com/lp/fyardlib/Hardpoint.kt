package com.lp.fyardlib

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Hardpoint(
    val id: String,
    val type: String,
    val `class`: String,
    val size: String,
    val quantity: Int,
    val mounts: Int,
    val details: String,
    val category: String?,
    val defaultEmpty: Boolean,
    val component: Component
): Parcelable