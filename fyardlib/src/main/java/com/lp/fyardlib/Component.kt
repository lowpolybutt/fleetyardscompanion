package com.lp.fyardlib

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Component(
    val id: String,
    val name: String,
    val slug: String,
    val size: String,
    val `class`: String,
    val type: String,
    val manufacturer: Manufacturer
): Parcelable