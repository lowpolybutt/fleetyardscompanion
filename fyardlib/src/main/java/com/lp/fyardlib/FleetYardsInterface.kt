package com.lp.fyardlib

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FleetYardsInterface {

    @GET("models")
    suspend fun getAllShips(@Query("perPage") perPage: Int, @Query("page") page: Int): List<BaseShipModel>

    @GET("models/slugs")
    fun getAllSlugs(): Call<List<String>>

    @GET("models/{slug}")
    fun getShipForSlug(@Path("slug") slug: String): Call<BaseShipModel>
}