package com.lp.fyardlib

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Manufacturer(
    val name: String,
    val slug: String,
    val code: String,
    val logo: String
): Parcelable