package com.lp.fleetyardscompanion.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.SharedElementCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lp.fleetyardscompanion.MainActivity
import com.lp.fleetyardscompanion.R
import com.lp.fleetyardscompanion.adapters.ShipListAdapter
import com.lp.fleetyardscompanion.dialogs.IndeterminateProgressDialog
import com.lp.fyardlib.BaseShipModel
import com.lp.fyardlib.FleetYardsInterface
import com.lp.fyardlib.Utils
import kotlinx.coroutines.CompletionHandler
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainFragment : Fragment() {

    private val ships = ArrayList<BaseShipModel>()
    private var pageReached: Int = 1

    private lateinit var rv: RecyclerView

    private suspend fun getShipsForPageNumber(pageNumber: Int): List<BaseShipModel> {
        val r = Utils.getRetrofitInstance()
        val api = r.create(FleetYardsInterface::class.java)
        return api.getAllShips(200, pageNumber)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setExitSharedElementCallback(object : SharedElementCallback() {
            override fun onMapSharedElements(
                names: MutableList<String>?,
                sharedElements: MutableMap<String, View>?
            ) {
                val selectedHolder = rv.findViewHolderForAdapterPosition(MainActivity.holderPosition)
                    as ShipListAdapter.ShipViewHolder?

                if (selectedHolder == null || selectedHolder.itemView == null) {
                    return
                }

                sharedElements?.put(names?.get(0)!!,
                    selectedHolder.itemView.findViewById(R.id.item_ship_recycler_image))
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv = view.findViewById(R.id.fragment_main_rv)
        rv.layoutManager = LinearLayoutManager(requireContext())
        rv.adapter = ShipListAdapter(ships, requireContext(), this)

        val pb = IndeterminateProgressDialog()

        rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    if (pageReached < 2) {
                        pb.show(requireFragmentManager(), "PB_")
                        MainScope().launch {
                            ships.addAll(getShipsForPageNumber(++pageReached))
                        }.invokeOnCompletion {
                            rv.adapter?.notifyDataSetChanged()
                            pb.dismiss()
                        }
                    }
                }
            }
        })

        pb.show(requireFragmentManager(), "PB")
        MainScope().launch {
            ships.addAll(getShipsForPageNumber(pageReached))
        }.invokeOnCompletion {
            rv.adapter?.notifyDataSetChanged()
            pb.dismiss()
        }
    }
}